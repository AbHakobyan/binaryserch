﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySerch
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 12,32,316,51,561,12,1,4165,156,456,1216,121,24,3254,3242,14,524,56,1,42,1245,3456,4657,35};
            DateTime startTime = DateTime.Now;
            Sort(array);
            DateTime endTime = DateTime.Now;
            Console.WriteLine(endTime - startTime);
            DateTime startTimeb = DateTime.Now;
            BinarySerching(array,1216);
            DateTime endTimeb = DateTime.Now;
            Console.WriteLine(endTimeb - startTimeb);
        }

        static void Sort(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        int temp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }

        static void BinarySerching(int[] array,int count)
        {
            int low = 0;
            int high = array.Length - 1;
            while (low <= high)
            {
                int mid = (low + high) / 2;
                int result = array[mid];
                if (result == count)
                    Console.WriteLine(mid);
                if (result > count)
                    high = mid - 1; 
                else
                    low = mid + 1;
            }
        }
    }
}
